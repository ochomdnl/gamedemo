﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour 
{
    GameObject pauseMenu;

    public bool paused;

    void Start()
    {
        paused = false;
        pauseMenu = GameObject.Find("PauseMenuCanvas");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
        }


        if (paused)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
        else if (!paused)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
    }


    public void Resume()
    {
        paused = false;
    }

    public void MainMenu(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Save()
    {
        PlayerPrefs.SetInt("currentSceneNumber", Application.loadedLevel);
    }

    public void Load()
    {
        Application.LoadLevel(PlayerPrefs.GetInt("currentSceneNumber"));
    }

    public void PauseGame()
    {
        paused = !paused;
    }
}
