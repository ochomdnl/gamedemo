﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject deathParticle;

    public PlayerController player;


    public float respawnDelay;


    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }


    void Update()
    {

    }


    public void RespawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }



    public IEnumerator RespawnPlayerCo()
    {

        Instantiate(deathParticle, player.transform.position, player.transform.rotation);
        player.GetComponent<Renderer>().enabled = false;
        //player.Death();

        yield return new WaitForSeconds(respawnDelay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
