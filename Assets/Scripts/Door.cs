﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour 
{
    [SerializeField]
    private string sceneNameToLoad;


    IEnumerator ChangeLevel()
    {
        float fadeTime = GameObject.Find("FadeIn").GetComponent<FadeInScene>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneCalling();
        
    }

    void SceneCalling()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            SceneCalling();
        }
    }

}
