﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour 
{

    public GameObject doorToApper;
    public float sec = 1.0f;


    public AudioSource coinPickUpSound;


    void Start()
    {
        if (doorToApper.activeInHierarchy)
            doorToApper.SetActive(false);
    
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
            doorToApper.SetActive(true);
            coinPickUpSound.Play();
        }
    }




    public IEnumerator LateCall()
    {
        yield return new WaitForSeconds(sec);
                      
    }


}
