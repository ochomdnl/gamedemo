﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour 
{
    public float moveSpeed;

    public bool moveRight;

    Rigidbody2D enemyRigidbody;


    public Transform wallCheckPoint;
    public float wallCheckRadius;
    public LayerMask wallLayer;
    private bool isTouchingWall;

	void Start () 
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();
	}

	void Update () 
    {

        isTouchingWall = Physics2D.OverlapCircle(wallCheckPoint.position, wallCheckRadius, wallLayer);

        if (isTouchingWall)
        {
            moveRight = !moveRight;
        }


        if (moveRight)
        {
            transform.localScale = new Vector3(-5.0f, 5.0f, 5.0f);
            enemyRigidbody.velocity = new Vector2(-moveSpeed, enemyRigidbody.velocity.y);
        }
        else
        {
            transform.localScale = new Vector3(5.0f, 5.0f, 5.0f);
            enemyRigidbody.velocity = new Vector2(moveSpeed, enemyRigidbody.velocity.y);
        }
		
	}
}
