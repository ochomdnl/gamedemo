﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour 
{


    public GameObject enemyDeathObject;
    public float bounceOnEnemy;
    private Rigidbody2D myOtherRigidbody;

    void Start()
    {
        myOtherRigidbody = transform.parent.GetComponent<Rigidbody2D>();
    }


    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            Instantiate(enemyDeathObject, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            myOtherRigidbody.velocity = new Vector2(myOtherRigidbody.velocity.x, bounceOnEnemy);
        }
    }
}
