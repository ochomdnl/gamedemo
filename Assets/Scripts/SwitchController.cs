﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour 
{
    public Sprite switchOff;
    public Sprite switchOn;

    private SpriteRenderer switchRenderer;
    public bool switchIsOn;

    public GameObject groundAppear;
    public GameObject groundDisappear;


	void Start () 
    {
        switchRenderer = GetComponent<SpriteRenderer>();

        if (groundDisappear.activeInHierarchy)
        {
            groundDisappear.SetActive(true);
        }
                    
	}
	

	void Update () 
    {
		
	}


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            switchRenderer.sprite = switchOn;
            switchIsOn = true;

            groundAppear.SetActive(true);

            groundDisappear.SetActive(false);
        }
    }
}
