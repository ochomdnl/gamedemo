﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    Animator anim;
    Rigidbody2D rb;

    public float speedX;
    public float jumpSpeedY;

    bool facingRight, jumping;
    float speed;


    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask groundLayer;
    private bool isTouchingGround;
    public bool canMoveInAir = true;

    public PauseMenu pauseGamePlay;

    //[SerializeField]
    //private string sceneNameToLoad;



	void Start () 
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        facingRight = true;

        pauseGamePlay = FindObjectOfType<PauseMenu>();
	}
	

	void Update () 
    {

        isTouchingGround = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayer);

        MovePlayer(speed);
        

        FlipPlayer();
        HandleJumpAndFall();

        //moving player to left
        if (Input.GetKeyDown(KeyCode.A))
        {
            speed = -speedX;
            anim.SetInteger("State", 2);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            speed = 0;
            anim.SetInteger("State", 0);
        }

        //moving player tothe right
        if (Input.GetKeyDown(KeyCode.D))
        {
            speed = speedX;
            anim.SetInteger("State", 2);
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            speed = 0;
            anim.SetInteger("State", 0);
        }


        //player jumping

        if (Input.GetKeyDown(KeyCode.UpArrow) && isTouchingGround)
        {
            IsJumping();
        }

	}


    void MovePlayer(float playerSpeed)
    {
        
        
        if(!canMoveInAir && !isTouchingGround)
            return;
        Vector2 MoveVel = rb.velocity;
        MoveVel.x = speedX * playerSpeed;
        rb.velocity = MoveVel;

 
     

        if (playerSpeed < 0 && !jumping || playerSpeed > 0 && !jumping)
        {
            anim.SetInteger("State", 2);
        }
        if (playerSpeed == 0 && !jumping)
        {
            anim.SetInteger("State", 0);
        }
    }


    void FlipPlayer()
    {
        if (speed > 0 && !facingRight || speed < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }


    void IsJumping()
    {
        jumping = true;
        rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
        anim.SetInteger("State", 3);
    }

    void HandleJumpAndFall()
    {
        if (jumping)
        {
            if (rb.velocity.y > 0)
            {
                anim.SetInteger("State", 3);
            }
            else
            {
                anim.SetInteger("State", 1);
            }
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            jumping = false;
            anim.SetInteger("State", 0);

        }


        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }

    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }



    //Mobile Controls 

    public void walkLeft()
    {
        speed = -speedX;
    }
    public void walkRight()
    {
        speed = speedX;
    }
    public void stopMovement()
    {
        speed = 0;
    }

    public void Jump()
    {
        if (isTouchingGround)
        {
            IsJumping();
        }
        
    }

    public void PauseGameButton()
    {
        pauseGamePlay.paused = true;
    }


    public void Death()
    {
         rb.velocity = Vector2.zero;
         Destroy(gameObject);      
    }
}
