﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInScene : MonoBehaviour 
{
    public Texture2D fadeOutTexture;
    public float fadeOutSpeed = 0.8f;

    private int drawDepth = -1000;
    private float alpha = 1.0f;
    private int fadeDirection = -1;


    void OnGUI()
    {
        alpha += fadeDirection * fadeOutSpeed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }


    //set fade direction

    public float BeginFade(int direction)
    {
        fadeDirection = direction;
        return (fadeOutSpeed);
    }

    // load level called 

    void OnLevelWasLoaded()
    {
        BeginFade(-1);
    }
}
